/**
 * Created by atavdi on 11/11/14.
 */
widget.on('render',function(w,e){
    e.widgetIds = [
        {id:'54621260136c82ccc3000283', panelToChange:'kpi', index:1},
        {id:'54621260136c82ccc3000282', panelToChange:'kpi', index:1}
    ];
    e.dimensions = [
        {
            "table":"V_F_TRAFFIC",
            "column":"MSR_GMB_FAM2_USD_AMOUNT",
            "dim":"[V_F_TRAFFIC.MSR_GMB_FAM2_USD_AMOUNT]",
            "datatype":"numeric",
            "agg":"sum",
            "title":"Total MSR_GMB_FAM2_USD_AMOUNT"
        },
        {
            "table":"V_F_TRAFFIC",
            "column":"MSR_GMB_FAM3_USD_AMOUNT",
            "dim":"[V_F_TRAFFIC.MSR_GMB_FAM3_USD_AMOUNT]",
            "datatype":"numeric",
            "agg":"sum",
            "title":"Total MSR_GMB_FAM3_USD_AMOUNT"
        },
    ]
})