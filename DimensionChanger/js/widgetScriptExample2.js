
widget.on('render',function(w,e){
    e.prefixText = 'Switch Between - ';
    e.widgetIds = [
     {
		id:'58a4c15a1ecad64c4b00005a', 
		panelToChange:'categories', 
		index:0
	 },
	{
		id:'58a4cb021ecad64c4b00006a', 
		panelToChange:'values', 
		index:0,
		itsComplicated: {
			"type": "measure",
			"formula": "([00000-001],[00000-002])",
			"context": {
				"[00000-001]": {
					"dim": "[Order.LineTotal1]",
					"datatype": "numeric",
					"agg": "sum"					
				},
				"[00000-002]": {
					"table": "@table",
					"column": "@column",
					"dim": "@dim",
					"datatype": "@datatype",
					"filter": {
						"top": 5,
						"by": {
							"dim": "[Order.LineTotal1]",
							"datatype": "numeric",
							"agg": "sum"
						}
					}
				}
			},
			"title": "Top 5"
		}
	 },
	 {
		id:'58a4cb021ecad64c4b00006a', 
		panelToChange:'values', 
		index:1,
		itsComplicated: {
			"type": "measure",
			"formula": "[00000-003] - ([00000-003],[00000-004])",
			"context": {
				"[00000-003]": {
					"dim": "[Order.LineTotal1]",
					"datatype": "numeric",
					"agg": "sum"					
				},
				"[00000-004]": {
					"table": "@table",
					"column": "@column",
					"dim": "@dim",
					"datatype": "@datatype",					
					"filter": {
						"top": 5,
						"by": {
							"dim": "[Order.LineTotal1]",
							"datatype": "numeric",
							"agg": "sum"
						}
					}
				}
			},
			"title": "All Others"
		}
	 }
    ];
});

