/**
 * @fileOverview    Dimensions Changer
 * @author          Dana Raijman
 * @version         V1.0
 * @SiSenseVersion  V5.7.6
 **/
prism.registerWidget("DimensionChanger", {
	name : "DimensionChanger",
	family : "DimensionChanger",
	title : "Dimension Changer",
	iconSmall : "/plugins/DimensionChanger/styles/dimension.png",
	styleEditorTemplate : '/plugins/DimensionChanger/js/styler.html',
	style : {},
	sizing: {// sizing must be stated
		minHeight: 32, //header
		maxHeight: 2048,
		minWidth: 128,
		maxWidth: 2048,
		height:32,
		defaultWidth: 512
	},
	data : {
		selection : [],
		defaultQueryResult : {},
		panels : [
            {//dimension panel
                name : 'rows',
                type : 'visible',
                metadata : {
                    types : ['dimensions'],
                    maxitems : -1
                }
            }, {//measure panel
                name : 'values',
                type : 'visible',
                metadata : {
                    sortable: true,
                    types : ['measures'],
                    maxitems : -1
                }
            },
            {
                name: 'filters',
                type: 'filters',
                metadata: {

                    types: ['dimensions'],
                    maxitems: -1
                }
            }
		],
		allocatePanel : function (widget, metadataItem) {
            if (prism.$jaql.isMeasure(metadataItem) && widget.metadata.panel("values").items.length === 0) {
                return "values";
            }
            // item
            else if (!prism.$jaql.isMeasure(metadataItem) && widget.metadata.panel("rows").items.length < 3) {
                return "rows";
            }
		},
		// returns true/ reason why the given item configuration is not/supported by the widget
		isSupported : function (items) {
			return this.rankMetadata(items, null, null) > -1;
		},
		// ranks the compatibility of the given metadata items with the widget
		rankMetadata : function (items, type, subtype) {
            var a = prism.$jaql.analyze(items);

            // require at least 2 dimensions of lat and lng and 1 measure
            if (a.dimensions.length >= 1 || a.measures.length >= 1) {
                return 0;
            }

            return -1;
		},
		// populates the metadata items to the widget
		populateMetadata : function (widget, items) {
            var a = prism.$jaql.analyze(items);

            // allocating dimensions
            widget.metadata.panel("rows").push(a.dimensions);
            widget.metadata.panel("values").push(a.measures);
            widget.metadata.panel("filters").push(a.filters);
		},
		// builds a jaql query from the given widget
		buildQuery : function (widget) {
            var query = {
                datasource : widget.datasource,
                metadata : []
            };

            // pushing non-KPI location items
            widget.metadata.panel("rows").items.forEach(function (item) {
                query.metadata.push(item);
            });

            // pushing KPI items
            widget.metadata.panel("values").items.forEach(function(item) {
                query.metadata.push(item)
            });

            // filters
            widget.metadata.panel("filters").items.forEach(function (item) {
                item = $$.object.clone(item, true);
                item.panel = "scope";
                query.metadata.push(item);
            });

            return query;
		},
		// prepares the widget-specific query result from the given result data-table
		processResult : function (widget, queryResult) {
			return queryResult;
		}
	},
	render : function (widget, e) {
        var dimensions;
        var lastSelected;
        var selectedColor  = e.selectedColor || '#86b817';
        var fontColor  = e.fontColor || '#cccccc';
        var descColor = e.descColor || "#a5a5a5";
        var prefixText = e.prefixText || '';
        var suffixText = e.suffixText || '';
        var parentWidth = $$get(widget, 'style.width') || e.elementWidth || '103%';
        var parentMarginLeft = e.parentMarginLeft || '-15px';
        var height = e.height || 32;
        var widgetIds;

        if(e.widgetIds){
            widgetIds = e.widgetIds;
        }
        else{
            return;
        }

        if(e.dimensions){
            dimensions = e.dimensions;
        }
        else{
            dimensions = _.map(widget.rawQueryResult.metadata, function(md){return md.jaql});

        }

        function isBlank(str) {
            return (!str || /^\s*$/.test(str));
        }

        var $firstItem;
        var desc = prefixText + ' '+ (widget.desc ? widget.desc : '');
        var $desc = $('<span class="descDefaultCSS" >' + desc + '&nbsp &nbsp'+ '</span>').css('color', descColor);
        var dimensionList = $('<div class="listDefaultCSS"></div>');

        if(desc && !isBlank(desc)){
            dimensionList.append($desc);
        }

        for (var i =0; i < dimensions.length; i++) {
            var $listItem = $('<span class="listItemDefaultCSS">' + dimensions[i].title + '</span>');

            $listItem.attr('tag',dimensions[i].title);

            if (!$firstItem){
                $firstItem = $listItem;
            }

            if(dimensions[i].default){
                select($listItem);
            }

            dimensionList.append($listItem);

            var $vSeparator = $('<div class="vSeparatorCSS"></div>');

            (i != dimensions.length-1) && dimensionList.append($vSeparator);
        }

        if (!defined(lastSelected)){
            select($firstItem);
        }

        if(suffixText.length){
            var $suffixText = $('<span class="descDefaultCSS" >' + '&nbsp' + suffixText + ''+ '</span>').css('color', descColor);

            dimensionList.append($suffixText)
        }

		var $lmnt = $(e.element);

        $lmnt.parent().addClass('parentCSS');
        $lmnt.parent().css('marginLeft', parentMarginLeft);
        $lmnt.empty().addClass('defaultCSS').append(dimensionList);
        $lmnt.css('height', height);
        $lmnt.css('color', fontColor);
        $lmnt.css('width', parentWidth);

        function dimensionClicked(){
            select(this);
            changeDimension(this);
        }

        function jaqlGenerator(jaql,dim){

            var newJaql = {};

            //  Loop through each attribute
            for (key in jaql){

                //  Figure out the type of the attribute
                var type = typeof jaql[key];

                //  Handle various scenarios
                if (type == "object"){
                    //  Nested Object
                    newJaql[key] = jaqlGenerator(jaql[key],dim);
                } else if ((type == "boolean") || (type == "number")) {
                    //  Just copy the number or boolean
                    newJaql[key] = jaql[key];
                } else if (type == "string"){
                    //  Check for parameters
                    var isParam = jaql[key].charAt(0) == "@";
                    
                    if (isParam){
                        //  Get the attribute key
                        var attr = jaql[key].substring(1,jaql[key].length);
                        //  Try and pass in a paramter from the current dimension
                        newJaql[key] = attr ? dim[attr] : jaql[key];
                    } else {
                        //  Not a parameter, just copy the string
                        newJaql[key] = jaql[key];
                    }
                } 
            }

            return newJaql
        }

        function changeDimension(element){
            var dim = getDimensionsByCaption($(element).attr('tag'));

            if(!dim){
                return;
            }

            for(var i= widgetIds.length-1;i>=0;i--){
                
                //  Init settings
                var setting = widgetIds[i],
                    widgetId = setting.id;

                //  Get the widget obj
                var widgetToApplyOn = getWidgetById(widgetId);
                if(widgetToApplyOn) {
                    
                    //  Get the item we're changing
                    var item = widgetToApplyOn.metadata.panel(setting.panelToChange).items[setting.index];

                    if (setting.itsComplicated){                        
                        //  Adjusting the jaql                        
                        item.jaql = jaqlGenerator(setting.itsComplicated,dim);
                    } else {
                        //  Standard change, just swap the jaql
                        item.jaql = dim;
                    }
                    widgetToApplyOn.changesMade();
                    widgetToApplyOn.refresh();
                }
            }
        }

        function getDimensionsByCaption(title){
            for(var i = dimensions.length-1; i >=0; i--){
                if(dimensions[i].title == title){
                    return dimensions[i];
                }
            }
        }

        function getDimensionsByDim(dim){
            for(var i = dimensions.length-1; i >=0; i--){
                if(dimensions[i].dim == dim){
                    return dimensions[i];
                }
            }
        }

        function getWidgetById(widgetId){
            var widgets = $$get(widget,'dashboard.widgets.$$widgets');

            if(!widgets){
                console.log('unable to get widgets from dashboard');
                return;
            }

            for(var i = widgets.length-1; i >=0; i--){
                if(widgets[i].oid == widgetId){
                    return widgets[i];
                }
            }
        }

        function select(item){
            if(lastSelected){
                $(lastSelected).css('color',fontColor)
            }

            $(item).css('color',selectedColor);
            lastSelected =  $(item);
        }

        function addEvents(dimList){
            for (var i = dimList.length -1; i >= 0; i--) {
                $('span[tag="'+ dimList[i].title+'"]').on('click',dimensionClicked)
            }
        }

        addEvents(dimensions);
        changeDimension(lastSelected);
	},
	destroy : function (e) {}
});


