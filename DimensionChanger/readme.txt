+************************************************+
* SiSense Dimension Changer *
+************************************************+

-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------
Description:
-- Add the ability to change widgets dimension
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------

How to use:
-----------

1) Create a Dimension Changer custom widget
3) Edit a Dimension Changer widget script, configure the different dimension changing:
 *
 * example:
 * widget.on('render',function(w,e){
       e.widgetIds = [
           {id:'54621260136c82ccc3000283', panelToChange:'kpi', index:1},
           {id:'54621260136c82ccc3000282', panelToChange:'kpi', index:1}
       ];
       e.dimensions = [
           {
               "table":"V_F_TRAFFIC",
               "column":"MSR_GMB_FAM2_USD_AMOUNT",
               "dim":"[V_F_TRAFFIC.MSR_GMB_FAM2_USD_AMOUNT]",
               "datatype":"numeric",
               "agg":"sum",
               "title":"Total MSR_GMB_FAM2_USD_AMOUNT"
           },
           {
               "table":"V_F_TRAFFIC",
               "column":"MSR_GMB_FAM3_USD_AMOUNT",
               "dim":"[V_F_TRAFFIC.MSR_GMB_FAM3_USD_AMOUNT]",
               "datatype":"numeric",
               "agg":"sum",
               "title":"Total MSR_GMB_FAM3_USD_AMOUNT"
           },
       ]
   })
3) setting e to the configuration you'd like to change.
 *
 * example:
 *  widget.on('render',function(w, e){e.selectedColor = 'red';});



Parameters:

Key			        Description								Type							DefaultValue
----------------------------------------------------------------------------------------------------------------------
widgetIds		    List of Widgets 						Array of Widget (see Bellow)
dimensions    		List of jaqls (optional)    			Array of jaql
selectedColor		Selected tab color						String							'#86b817'
fontColor			Font color								String							'#cccccc'
prefixText			Prefix text for the tab list			String
suffixText			Suffix text for the tab list			String
elementWidth		Tab list width							String							'103%'
descColor			Tab description color					String							'#a5a5a5'
parentMarginLeft	Margin left if tab list from parent		String							'-15px'
height				Tab list height							Number							32


Widget:

Key			        Description					Type
-------------------------------------------------------
id					Widget id					String
panelToChange		Name of panel to change		String
index				Index of item in panel		Number


